package deloitte.academy.lesson01.interfaz;

import java.util.List;

import deloitte.academy.lesson01.abstractas.Producto;

/**
 * Interfaz con funciones CRUD
 * 
 * @author mvillegas
 *
 */
public interface Crud {
	public void createProducto(Producto producto);

	public void updateProducto(Producto producto, Producto cambio);

	public void deleteProducto(Producto producto);

	public void readProducto(Producto producto);

}
