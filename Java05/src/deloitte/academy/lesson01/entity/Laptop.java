package deloitte.academy.lesson01.entity;

import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.abstractas.Producto;
import deloitte.academy.lesson01.interfaz.Crud;
import deloitte.academy.lesson01.run.Main;

/**
 * Funcion Laptop hereda de Producto e implementa Crud
 * 
 * @author mvillegas
 *
 */
public class Laptop extends Producto implements Crud {
	private static final Logger LOGGER = Logger.getLogger(Laptop.class.getName());;

	public Laptop() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor que auto asigna el nombre
	 * 
	 * @param cantidad
	 * @param precio
	 * @param proveedor
	 */
	public Laptop(int cantidad, double precio, String proveedor) {
		super(cantidad, precio, proveedor);
		this.nombre = Nombre.Laptop;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con producto de parametro para crear auxiliares
	 * 
	 * @param p
	 */
	public Laptop(Producto p) {
		super(p);
		this.nombre = Nombre.Laptop;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con esta clase se crea un nuevo producto se agrega al stock y a la lista
	 * propia
	 */
	@Override
	public void createProducto(Producto producto) {
		// TODO Auto-generated method stub
		Main.listaLaptop.add((Laptop) producto);
		Main.listaStock.add(producto);
		LOGGER.info("Producto registrado satisfactoriamente");

	}

	/**
	 * Con esta funcion se actualiza un producto en la lista propia y en el stock
	 */
	@Override
	public void updateProducto(Producto producto, Producto cambio) {

		// TODO Auto-generated method stub
		Main.listaLaptop.remove(producto);
		Main.listaStock.remove(producto);
		Main.listaLaptop.add((Laptop) cambio);
		Main.listaStock.add(producto);
		LOGGER.info("Producto actualizado satisfactoriamente");

	}

	/**
	 * funcion que elimina un producto de la lista propia y del stock
	 */
	@Override
	public void deleteProducto(Producto producto) {
		// TODO Auto-generated method stub
		Main.listaLaptop.remove(producto);
		Main.listaStock.remove(producto);
		LOGGER.info("Producto registrado satisfactoriamente");

	}

	/**
	 * Funcion que imprime los parametros del producto
	 */
	@Override
	public void readProducto(Producto producto) {
		// TODO Auto-generated method stub
		System.out.println(this.toString());

	}

	@Override
	public String toString() {
		String status = "Nombre: " + this.nombre + "\nPrecio: " + this.precio + "\nProveedor: " + this.proveedor
				+ "\nStock: " + this.cantidad;
		return status;
	}

	/**
	 * Funcion que simula una venta y agrega el producto al stock de ventas
	 */
	@Override
	public void vender() {
		// TODO Auto-generated method stub
		try {
			if (Main.listaLaptop.contains(this)) {
				Laptop aux = new Laptop(this);
				this.setCantidad(this.cantidad - 1);
				aux.setCantidad(1);
				Main.listaLaptop.add(aux);
				LOGGER.info("Producto vendido satisfactoriamente");
			} else {
				LOGGER.info("Producto no encontrado");

			}

		} catch (Exception e) {
			LOGGER.severe("Error " + e);

		}

	}

	/**
	 * Aumenta el inventario con una unidad
	 */
	@Override
	public void aumentoInventario() {
		// TODO Auto-generated method stub
		this.cantidad++;

	}

}
