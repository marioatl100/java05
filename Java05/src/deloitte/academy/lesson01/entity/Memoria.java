package deloitte.academy.lesson01.entity;

import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.abstractas.Producto;
import deloitte.academy.lesson01.interfaz.Crud;
import deloitte.academy.lesson01.logic.Funciones;
import deloitte.academy.lesson01.run.Main;

/**
 * Clase memoria que hereda de Producto e implementa Crud
 * 
 * @author mvillegas
 *
 */
public class Memoria extends Producto implements Crud {
	private static final Logger LOGGER = Logger.getLogger(Memoria.class.getName());;

	public Memoria() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor que auto asigna el nombre
	 * 
	 * @param cantidad
	 * @param precio
	 * @param proveedor
	 */
	public Memoria(int cantidad, double precio, String proveedor) {
		super(cantidad, precio, proveedor);
		this.nombre = Nombre.Memoria;
		// TODO Auto-generated constructor stub
	}

	public Memoria(Producto p) {
		super(p);
		this.nombre = Nombre.Laptop;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con esta clase se crea un nuevo producto se agrega al stock y a la lista
	 * propia
	 */
	@Override
	public void createProducto(Producto producto) {
		// TODO Auto-generated method stub
		Main.listaMemoria.add((Memoria) producto);
		Main.listaStock.add(producto);
		LOGGER.info("Producto agregado satisfactoriamente");

	}

	/**
	 * Con esta funcion se actualiza un producto en la lista propia y en el stock
	 */
	@Override
	public void updateProducto(Producto producto, Producto cambio) {
		// TODO Auto-generated method stub
		Main.listaMemoria.remove(producto);
		Main.listaStock.remove(producto);
		Main.listaMemoria.add((Memoria) cambio);
		Main.listaStock.add(cambio);
		LOGGER.info("Producto actualizado satisfactoriamente");

	}

	/**
	 * funcion que elimina un producto de la lista propia y del stock
	 */
	@Override
	public void deleteProducto(Producto producto) {
		// TODO Auto-generated method stub
		Main.listaMemoria.remove(producto);
		Main.listaStock.remove(producto);
		LOGGER.info("Producto borrado satisfactoriamente");

	}

	/**
	 * Funcion que imprime los parametros del producto
	 */
	@Override
	public void readProducto(Producto producto) {
		// TODO Auto-generated method stub
		System.out.println(this.toString());

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String status = "Nombre: " + this.nombre + "\nPrecio: " + this.precio + "\nProveedor: " + this.proveedor
				+ "\nStock: " + this.cantidad;
		return status;
	}

	/**
	 * Funcion que simula una venta y agrega el producto al stock de ventas
	 */
	@Override
	public void vender() {
		try { // TODO Auto-generated method stub
			Memoria aux = new Memoria(this);
			this.setCantidad(this.cantidad - 1);
			aux.setCantidad(1);
			Main.listaMemoria.add(aux);
		} catch (Exception e) {
			LOGGER.severe("Error " + e);
		}
	}

	/**
	 * Aumenta el inventario con una unidad
	 */
	@Override
	public void aumentoInventario() {
		// TODO Auto-generated method stub
		this.cantidad++;

	}

}
