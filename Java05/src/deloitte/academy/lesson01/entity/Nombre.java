package deloitte.academy.lesson01.entity;
/**
 * Enum con los nombres de los procutos
 * @author mvillegas
 *
 */
public enum Nombre {
	Laptop, Audifono, Memoria;

}
