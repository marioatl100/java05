package deloitte.academy.lesson01.entity;

import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.abstractas.Producto;
import deloitte.academy.lesson01.interfaz.Crud;
import deloitte.academy.lesson01.run.Main;

/**
 * Clase audifonos que ereda de producto e implemetna Crud
 * 
 * @author mvillegas
 *
 */
public class Audifonos extends Producto implements Crud {
	private static final Logger LOGGER = Logger.getLogger(Audifonos.class.getName());;

	public Audifonos() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor que auto asigna el nombre
	 * 
	 * @param cantidad
	 * @param precio
	 * @param proveedor
	 */
	public Audifonos(int cantidad, double precio, String proveedor) {
		super(cantidad, precio, proveedor);
		this.nombre = Nombre.Audifono;
		// TODO Auto-generated constructor stub
	}

	public Audifonos(Producto p) {
		super(p);
		this.nombre = Nombre.Laptop;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Con esta clase se crea un nuevo producto se agrega al stock y a la lista
	 * propia
	 */
	@Override
	public void createProducto(Producto producto) {
		// TODO Auto-generated method stub
		Main.listaAudifono.add((Audifonos) producto);
		Main.listaStock.add(producto);
		LOGGER.info("Producto registrado satisfactoriamente");

	}

	/**
	 * Con esta funcion se actualiza un producto en la lista propia y en el stock
	 */
	@Override
	public void updateProducto(Producto producto, Producto cambio) {
		// TODO Auto-generated method stub
		Main.listaAudifono.remove(producto);
		Main.listaStock.remove(producto);
		Main.listaAudifono.add((Audifonos) cambio);
		Main.listaStock.add(producto);
		LOGGER.info("Producto actualizado satisfactoriamente");

	}

	/**
	 * funcion que elimina un producto de la lista propia y del stock
	 */
	@Override
	public void deleteProducto(Producto producto) {
		// TODO Auto-generated method stub
		Main.listaAudifono.remove(producto);
		Main.listaStock.remove(producto);
		LOGGER.info("Producto borrado satisfactoriamente");

	}

	/**
	 * Funcion que imprime los parametros del producto
	 */
	@Override
	public void readProducto(Producto producto) {
		// TODO Auto-generated method stub
		System.out.println(this.toString());

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String status = "Nombre: " + this.nombre + "\nPrecio: " + this.precio + "\nProveedor: " + this.proveedor
				+ "\nStock: " + this.cantidad;
		return status;
	}

	/**
	 * Funcion que simula una venta y agrega el producto al stock de ventas
	 */
	@Override
	public void vender() {
		// TODO Auto-generated method stub
		try {
			if (Main.listaAudifono.contains(this)) {
				Audifonos aux = new Audifonos(this);
				this.setCantidad(this.cantidad - 1);
				aux.setCantidad(1);
				Main.listaVendidos.add(aux);
			} else {
				LOGGER.info("Producto agotado");
			}
		} catch (Exception e) {
			LOGGER.severe("Error " + e);

		}
	}

	/**
	 * Aumenta el inventario con una unidad
	 */
	@Override
	public void aumentoInventario() {
		// TODO Auto-generated method stub
		this.cantidad++;

	}

}
