package deloitte.academy.lesson01.logic;

import java.util.List;

import deloitte.academy.lesson01.abstractas.Producto;
import deloitte.academy.lesson01.entity.Audifonos;
import deloitte.academy.lesson01.entity.Laptop;
import deloitte.academy.lesson01.entity.Memoria;

public class Funciones {
	public static void imprimeLaptop(List<Laptop> lista) {
		System.out.println("LISTA LAPTOPS");
		for (Laptop aux : lista) {
			aux.readProducto(aux);
			System.out.println("---------------------------------------------");
		}
		System.out.println("---------------------------------------------");

	}

	public static void imprimeAudifonos(List<Audifonos> lista) {
		System.out.println("LISTA AUDIFONOS");
		for (Audifonos aux : lista) {
			aux.readProducto(aux);
			System.out.println("---------------------------------------------");
		}
		System.out.println("---------------------------------------------");

	}

	public static void imprimeMemoria(List<Memoria> lista) {
		System.out.println("LISTA MEMORIAs");
		for (Memoria aux : lista) {
			aux.readProducto(aux);
			System.out.println("---------------------------------------------");
		}
		System.out.println("---------------------------------------------");

	}

	public static void imprimeLista(List<Producto> lista) {
		System.out.println("LISTA");
		for (Producto aux: lista) 
		{
			System.out.println(aux.toString());	
					System.out.println("---------------------------------------------");
		}
		System.out.println("---------------------------------------------");

	}

}
