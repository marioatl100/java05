package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson01.abstractas.Producto;
import deloitte.academy.lesson01.entity.Audifonos;
import deloitte.academy.lesson01.entity.Laptop;
import deloitte.academy.lesson01.entity.Memoria;
import deloitte.academy.lesson01.entity.Nombre;
import deloitte.academy.lesson01.logic.Funciones;
/**
 * Clase main con la funcionalidad del proyecto
 * @author mvillegas
 *
 */
public class Main {
	public static List<Laptop> listaLaptop = new ArrayList<Laptop>();
	public static List<Audifonos> listaAudifono = new ArrayList<Audifonos>();
	public static List<Memoria> listaMemoria = new ArrayList<Memoria>();
	public static List<Producto> listaStock = new ArrayList<Producto>();
	public static List<Producto> listaVendidos = new ArrayList<Producto>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Relleno de valoressfdasfdasdf
		 */
		Laptop laptop = new Laptop(50, 6000, "Lenovo");
		Audifonos a1 = new Audifonos(50, 200, "Beat");
		Memoria m1 = new Memoria(50, 200, "kingston");
		Audifonos a2 = new Audifonos(50, 180, "Beat4");
		Audifonos a3 = new Audifonos(50, 250, "Beat2");
		Audifonos a4 = new Audifonos(50,300,"Kingston4");
		laptop.createProducto(laptop);
		a1.createProducto(a1);
		m1.createProducto(m1);
		a1.createProducto(a2);
		a1.createProducto(a3);
		/**
		 * aumento de inventario en 50 piezas
		 */
		for (int i = 0; i < 50; i++) {
			m1.aumentoInventario();
		}

		// laptop.readProducto(laptop);
		// Funciones.imprimeLista(listaLaptop);
		/**
		 * Venta de 5 productos
		 */
		for (int i = 0; i < 5; i++) {
			a1.vender();
		}
		
		a1.updateProducto(a3, a4);
		laptop.deleteProducto(laptop);
		// Funciones.imprimeAudifonos(listaAudifono);
		Funciones.imprimeLista(listaVendidos);
		Funciones.imprimeLista(listaStock);

	}

}
