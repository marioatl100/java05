package deloitte.academy.lesson01.abstractas;

import deloitte.academy.lesson01.entity.Nombre;

/**
 * Clase abstracta producto que hereda los parametros nombre, cantidad, precio y
 * proveedor
 * 
 * @author mvillegas
 *
 */
public abstract class Producto {
	public Nombre nombre;
	public int cantidad;
	public double precio;
	public String proveedor;

	public Producto() {
		// TODO Auto-generated constructor stub
	}

	public Producto(Nombre nombre, int cantidad, double precio, String proveedor) {
		super();
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.precio = precio;
		this.proveedor = proveedor;
	}

	/**
	 * Constructor con objeto como parametro para ayuda con auxiliares
	 * 
	 * @param p
	 */
	public Producto(Producto p) {
		super();
		this.cantidad = p.cantidad;
		this.precio = p.precio;
		this.proveedor = p.proveedor;
	}

	public Producto(int cantidad, double precio, String proveedor) {
		super();
		this.cantidad = cantidad;
		this.precio = precio;
		this.proveedor = proveedor;
	}

	/**
	 * Funcion que simula una venta
	 */
	public abstract void vender();

	/**
	 * Funcion que simula un aumento en el inventario
	 */
	public abstract void aumentoInventario();

	public Nombre getNombre() {
		return nombre;
	}

	public void setNombre(Nombre nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	@Override
	public abstract String toString();

}
